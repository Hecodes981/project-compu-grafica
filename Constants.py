import pygame as pg

window_size = (860, 640)


#Colores
ROJO = (255,0,0)
VERDE = (0,255,0)
AZUL = (0,0,255)
BLANCO = (255,255,255)
NEGRO = (0,0,0)
AMARILLO = (255,255,0)

def recortarImagenes(img, size, scale):
    img_rect = img.get_rect()[2:]
    matriz = []
    filas = img_rect[1]/size[1]
    columnas = img_rect[0]/size[0]

    for i in range(filas):
        matriz.append([])
        for j in range(columnas):
            frame = img.subsurface((j*size[0]), i*size[1], size[0], size[1])
            matriz[i].append(frame)

    return matriz
